## @universis/ngx-query-builder

An implementation of [jQuery QueryBuilder](https://github.com/mistic100/jQuery-QueryBuilder) for [Universis project](https://gitlab.com/universis) applications.

## Installation

Install @universis/ngx-query-builder in an angular cli by executing:

    npm i @universis/ngx-query-builder

Important note: Don't forget to install peer dependencies also.

## Usage

Import `NgxQueryBuilderModule`:

    # app.module.ts
    import { NgxQueryBuilderModule } from '@universis/ngx-query-builder';
    ...
    imports: [
      CommonModule,
      BrowserModule,
      HttpClientModule,
      TranslateModule.forRoot(),
      SharedModule.forRoot(),
      RouterModule,
      ...
      ...
      NgxQueryBuilderModule.forRoot()
    ],

## Development

This project is intended to be used as a part of an angular cli project. 

- Add this project as git submodule

        git submodule add https://gitlab.com/universis/ngx-query-builder.git projects/ngx-query-builder

- Edit angular.json and add `ngx-query-builder` configuration under `projects` node:

        "ngx-query-builder": {
            "root": "projects/ngx-query-builder",
            "sourceRoot": "projects/ngx-query-builder/src",
            "projectType": "library",
            "prefix": "universis",
            "architect": {
                "build": {
                    "builder": "@angular-devkit/build-ng-packagr:build",
                    "options": {
                        "tsConfig": "projects/ngx-query-builder/tsconfig.lib.json",
                        "project": "projects/ngx-query-builder/ng-package.json"
                    }
                },
                "test": {
                "builder": "@angular-devkit/build-angular:karma",
                "options": {
                    "main": "projects/ngx-query-builder/src/test.ts",
                    "tsConfig": "projects/ngx-query-builder/tsconfig.spec.json",
                    "karmaConfig": "projects/ngx-query-builder/karma.conf.js"
                }
                },
                "lint": {
                "builder": "@angular-devkit/build-angular:tslint",
                "options": {
                    "tsConfig": [
                        "projects/ngx-query-builder/tsconfig.lib.json",
                        "projects/ngx-query-builder/tsconfig.spec.json"
                    ],
                    "exclude": [
                    "**/node_modules/**"
                    ]
                }
            }
        }
        }

- Import `@universis/ngx-query-builder` in tsconfig.json#paths

        {
            ...
            "compilerOptions": {
                ...
                "paths": {
                    ...
                    "@universis/ngx-query-builder": [
                        "packages/ngx-query-builder/src/public-api"
                    ]
                }
            }
        }

